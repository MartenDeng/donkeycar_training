from enum import Enum, auto
import time

class CarStates(Enum):
    DRIVING = auto()
    STOP = auto()
    LEAVE_STOP = auto()
    RED_TRAFFIC_STOP = auto()
    TURN_RIGHT = auto()


# putting this here so i can have a reference of all the sign names.
labels = ['amber_traffic_light', 'black_left', 'black_park', 'black_right', \
        'blue_left', 'blue_right', 'cone', 'green_park', 'green_traffic_light', \
        'red_traffic_light', 'signs', 'speed_10', 'speed_25', 'speed_40', 'speed_5', \
        'speed_50', 'stop_sign', 'yellow_park']

class SignResponder:  
    """ Sign responder part for donkey car
        Inputs: detected sign, throttle, steering
        Outputs: response/throttle, response/steering
    """

    def __init__(self):
        self.current_state = CarStates.DRIVING
        self.state_to_action = {
            CarStates.DRIVING : self.drive,
            CarStates.STOP: self.stop,
            CarStates.LEAVE_STOP: self.leave_stop,
            CarStates.RED_TRAFFIC_STOP: self.traffic_light_stop,
            CarStates.TURN_RIGHT: self.turn_right
        }
        # variable to help count how many seconds has passed since run function was previously called.
        self.prev_time = time.time() 
        self.time_in_state = 0

    def run(self, sign, throttle, steering):
        #print(throttle, steering)
        time_passed = time.time() - self.prev_time
        self.prev_time = time.time()
        return self.state_to_action[self.current_state](sign, throttle, steering, time_passed)

    def run_threaded(self, sign, throttle, steering):
        return self.run(sign, throttle, steering)

    def update():
        pass

    def drive(self, sign, throttle, steering, time_passed):
        print("drive state")

        if sign == "stop_sign":
            self.change_state(CarStates.STOP)

        elif sign == "red_traffic_light":
            self.change_state(CarStates.RED_TRAFFIC_STOP)
        
        elif sign == "black_right" or sign == "blue_right":
            self.change_state(CarStates.TURN_RIGHT)
        
        # TODO: finish add state for each sign.

        return None, None 

    def stop(self, sign, throttle, steering, time_passed):
        print("stop state")
        self.time_in_state += time_passed

        # continue after stopping for 5 seconds
        if (self.time_in_state >= 5):
            self.change_state(CarStates.LEAVE_STOP)

        if throttle > 0:
            return -1, 0
        
        if throttle < 0:
            if (self.time_in_state < 0.5): 
                return -1, 0
        
        return 0, 0
        
    def leave_stop(self, sign, throttle, steering, time_passed):
        """ This state exists so that you don't get stuck in stop"""
        print("leave stop")
        self.time_in_state += time_passed

        if (self.time_in_state > 7):
            self.change_state(CarStates.DRIVING)
        
        return None, None

    def turn_right(self, sign, throttle, steering, time_passed):
        print("turn right")
        self.time_in_state += time_passed

        if (self.time_in_state > 1.5):
            self.change_state(CarStates.DRIVING)

        return throttle, 1

    def turn_left(self, sign, throttle, steering, time_passed):
        print("turn left")

        self.time_in_state += time_passed

        if (self.time_in_state > 1.5):
            self.change_state(CarStates.DRIVING)

        return throttle, -1

    def traffic_light_stop(self, sign, throttle, steering, time_passed):
        print("traffic light")

        if (sign == "green_traffic_light" or sign == "None"):
            self.change_state(CarStates.DRIVING)
            return None, None
        
        self.time_in_state += time_passed

        # brake
        if throttle > 0:
            return -1, 0
        
        if throttle < 0:
            if (self.time_in_state < 0.25):
                return -1, 0
        
        return 0, 0

    def change_state(self, new_state):
        self.time_in_state = 0
        self.current_state = new_state

    