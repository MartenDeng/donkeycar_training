#!/usr/bin/env python3
"""
Scripts for operating the OpenCV by Robotics Masters
 with the Donkeycar
author: @wallarug (Cian Byrne) 2020
contrib: @peterpanstechland 2020

Note: To be used with code.py bundled in this repo. See donkeycar/contrib/robohat/code.py
"""

import time
import donkeycar as dk
import cv2

#TODO:  Import the working OpenCV libraries here
from rmracerlib.cv.lights import detect_traffic
from rmracerlib.cv.signs import detect_stop, detect_park, detect_turn

import tensorflow as tf

import numpy as np

COUNTER_THRESHOLD = 4   # will take (+1) of value
COUNTER_SIZE = 10       # 1 = ~0.05 secs, 20 = 1 sec

ACTION_BUFFER = 10      # 10 = 0.5s, 20 = 1 sec
ACTION_TIME = 60        # 60 = 3.0s, 20 = 1 sec

LOAD_TIME_WAIT_SEC = 10

class RMRacerCV:
    '''
    Documentation to be added later

    '''
    def __init__(self, cfg, debug=False):
        self.throttle = 0
        self.steering = 0
        self.debug = debug
        #self.show_bounding_box = cfg.show_bounding_box

        self.ready = False

        # actions for the car & error detection
        self.stop_sign = Queue10(COUNTER_SIZE)
        self.right_sign = Queue10(COUNTER_SIZE)
        self.left_sign = Queue10(COUNTER_SIZE)
        self.park_sign = Queue10(COUNTER_SIZE)
        self.red_traf = Queue10(COUNTER_SIZE)
        self.grn_traf = Queue10(COUNTER_SIZE)

        # if we are already running a sequence (like parking) do
        #  not do anything except finish that sequence
        self.running = False
        self.action = None

        self.cfg = cfg

        self.sign_classifier = tf.keras.models.load_model("complete-sign-detector.h5")

    def run(self, img_arr, throttle, steering, debug=False):
        if img_arr is None:
            return throttle, steering, img_arr

        ## pre-process the image to save time..
        labels = ['amber_traffic_light', 'black_left', 'black_park', 'black_right', 'blue_left', 'blue_right', 'cone', 'green_park', 'green_traffic_light', 'red_traffic_light', 'signs', 'speed_10', 'speed_25', 'speed_40', 'speed_5', 'speed_50', 'stop_sign', 'yellow_park']
        image = tf.expand_dims(img_arr, 0)
        prediction = self.sign_classifier.predict(image)
        confidence = tf.reduce_max(tf.nn.softmax(prediction))
        sign = labels[np.argmax(prediction)]
        
        # I don't want to return a string but for some reason but when 
        # i return None, it is not picked up by the other parts.
        detected_sign = "None"

        if confidence > 0.99:
            detected_sign = sign
            cv2.putText(img_arr, sign, (50, 50), cv2.FONT_HERSHEY_SIMPLEX, 1, (255,127,0))
        
        img_arr = cv2.cvtColor(img_arr, cv2.COLOR_BGR2RGB) # convert to RGB
        
        cv2.imshow("car view", img_arr)
        cv2.waitKey(1)
        
        return detected_sign
        ##
        ##  DETECTION CODE
        ##
        # Stop Sign (Detect)
        # stop = detect_stop(img_arr, hsv)

        # if stop == "stop":
        #     # add one to counter, check if above threshold
        #     if self.stop_sign.put(1) > COUNTER_THRESHOLD:
        #         self.running = True
        #         self.action = "stopping"
        #         return throttle, steering, img_arr
        # else:
        #     self.stop_sign.put()

        # # Turn Sign (Detect)
        # turn = detect_turn(img_arr, hsv)

        # if turn == "right":
        #     if self.right_sign.put(1) > COUNTER_THRESHOLD:
        #         self.running = True
        #         self.action = "right"
        #         return throttle, steering, img_arr

        # elif turn == "left":
        #     if self.left_sign.put(1) > COUNTER_THRESHOLD:
        #         self.running = True
        #         self.action = "left"
        #         return throttle, steering, img_arr
        # else:
        #     self.right_sign.put()
        #     self.left_sign.put()

        # # Traffic Light (Detect)
        # traffic_light = detect_traffic(hsv)

        # if traffic_light == "stop":
        #     if self.red_traf.put(1) > COUNTER_THRESHOLD:
        #         self.running = True
        #         self.action = "lstopping"
        #         return throttle, steering, img_arr
        # elif traffic_light == "go":
        #     self.grn_traf.put(1)
        # else:
        #     self.red_traf.put()
        #     self.grn_traf.put()

        # # Park Sign (Detect)
        # park = detect_park(img_arr, hsv)

        # if park == "park":
        #     if self.park_sign.put(1) > COUNTER_THRESHOLD:
        #         self.running = True
        #         self.action = "park"
        #         return throttle, steering, img_arr
        # else:
        #     self.park_sign.put()

        # ##
        # ## Action Check Status
        # ##
    """
        if self.running == True:
            # stop sign & traffic light actions
            if (self.action == "stopping" or self.action == "lstopping") and self.wait < ACTION_BUFFER:  # wait before stopping
                self.wait += 1
                return throttle, steering, img_arr
            elif (self.action == "stopping" or self.action == "lstopping") and self.wait > ACTION_BUFFER: # execute
                self.wait = 0
                if self.action == "lstopping":
                    self.action = "waiting"
                else:
                    self.action = "stopped"
                return 0, steering, img_arr

            # stop sign only
            elif self.action == "stopped" and self.wait < ACTION_TIME:  # execute, action is running
                self.wait += 1
                return 0, steering, img_arr
            elif self.action == "stopped" and self.wait > ACTION_TIME:  # complete, leave action
                self.wait = 0
                self.action = None
                self.running = False
                self.stop_sign.clear()
                return throttle, steering, img_arr

            # traffic light only
            elif self.action == "waiting": # minimum wait time?: and self.wait < ACTION_TIME:
                hsv = cv2.cvtColor(img_arr, cv2.COLOR_BGR2RGB) # convert to HSV CS
                traffic_light = detect_traffic(hsv)

                if traffic_light == "go":
                    self.grn_traf.put(1)
                    if self.grn_traf.total > COUNTER_THRESHOLD:
                        # we can move again
                        self.wait = 0
                        self.action = None
                        self.running = False
                        self.red_traf.clear()
                        return throttle, steering, img_arr
                else:
                    self.grn_traf.put()
                    self.wait += 1
                    return 0, steering, img_arr

            # turn only
            elif (self.action == "left" or self.action == "right") and self.wait < ACTION_BUFFER:
                self.wait += 1
                return throttle, steering, img_arr

            elif (self.action == "left" or self.action == "right") and self.wait > ACTION_BUFFER:
                if self.action == "left" and self.wait < (ACTION_BUFFER + ACTION_TIME):
                    self.wait += 1
                    return throttle, -1.0, img_arr
                elif self.action == "right" and self.wait < (ACTION_BUFFER + ACTION_TIME):
                    self.wait += 1
                    return throttle, 1.0, img_arr
                else:
                    self.wait = 0
                    self.action = None
                    self.running = False
                    self.left_sign.clear()
                    self.right_sign.clear()
                    return throttle, steering, img_arr

            # park only
            elif self.action == "park" and self.wait < ACTION_BUFFER:
                self.wait += 1
                return throttle, steering, img_arr
            elif self.action == "park" and self.wait > ACTION_BUFFER:
                self.wait = 0
                self.action = "parking"
                return 0, steering, img_arr
            elif self.action == "parking":

                # action for parking the car (over time)
                ##  To accomplish this, we have to send different values over various ticks
                ##   since using sleep freezes the Donkey Car.
                ##  We shall define ticks as 20 every second.  The whole process will take
                ##   120 ticks (approx) or 6 seconds
                ##
                if self.wait < 40:
                    # first we must steer the car into the parking spot, assuming we know
                    #  which side the park sign is on (TODO)
                    return -0.5, 1.0, img_arr # lock left, reverse parking?

                elif self.wait < 60:
                    return -0.5, 0.0, img_arr # straighten up, reverse parking?

                elif self.wait > 100:
                    return 0.0, 0.0, img_arr  # stop the car

                elif self.wait > 200:
                    self.wait = 0
                    self.action = None
                    self.running = False
                    self.park_sign.clear()
                    return throttle, steering, img_arr
    """

    def update(self):
        pass

    def run_threaded(self, img_arr, throttle, steering):
        return self.run(img_arr, throttle, steering)



class Queue10:
    '''
    A Stack that only holds 10 items, nothing more, nothing less
    '''
    def __init__(self, size):
        self._list = [0] * size
        self._index = 0
        self.size = size
        self.total = 0

    def put(self, value=0):
        self._list[self._index] = value
        self._increment()
        return self.total

    def _increment(self):
        if self._index == self.size - 1:
            self._index = 0
        else:
            self._index += 1
        self.total = sum(self._list)

    def clear(self):
        while(i < self.size):
            self.put(0)

    def sneaky(self):
        return self._list



# Traffic Light Detector
class TrafficLightDetector:
    '''
    Documentation to be added later

    '''

    def __init__(self, cfg, debug=False):
        self.throttle = None
        self.detectcounter = 0
        self.mode = 'user'
        self.debug = debug
        self.ready = False

    def shutdown(self):
        try:
            pass
        except:
            pass

    def detector(self, img_arr):
        '''
        Detects if there is a traffic light in given frame and then returns a throttle value.
        '''
        # run the image through the detector
        result = detect_traffic(img_arr)

        # add counter if yes
        if result == "stop":
            self.detectcounter += 1

        # reset counter when green
        if result == "go":
            self.detectcounter = 0
            self.throttle = None

        # check for action
        if self.detectcounter >= COUNTER_THRESHOLD:
            self.throttle = 0.0  # no throttle is stopped

        return self.throttle

    def update(self):
        print("camera not ready! wait for traffic lights")
        time.sleep(LOAD_TIME_WAIT_SEC)  # wait for camera to load
        self.ready = True  # set flag that camera is ready
        print("camera ready! starting traffic lights")
        # this loop does stuff.

    def run(self, img_arr = None):
        return self.run_threaded()

    def run_threaded(self, img_arr = None):
        if self.ready: return self.detector(img_arr)
        else: return None


# Stop Sign Detector
class TrafficSignStopDetector:
    '''
    Documentation to be added later

    '''

    def __init__(self, cfg, debug=False):
        self.throttle = None
        self.detectcounter = 0
        self.mode = 'user'
        self.debug = debug
        self.ready = False
        self.tick = 0
        self.waiting = False

    def shutdown(self):
        try:
            pass
        except:
            pass

    def detector(self, img_arr):
        '''
        Detects if there is a stop sign in given frame and then returns a throttle value.
        '''
        # run the image through the detector
        result = detect_stop(img_arr)
        self.tick += 1

        # detections - add counter if yes
        if result == "stop" and not self.waiting:
            self.detectcounter += 1

        # false detections - reset counter when no detections after 0.25 seconds
        if result == None and self.detectcounter < COUNTER_THRESHOLD and self.tick > 8:
            self.detectcounter = 0
            self.tick = 0
            self.throttle = None

        # positive detections - check for action
        if self.detectcounter >= COUNTER_THRESHOLD or self.waiting:
            self.throttle = 0.0  # throttle is stopped
            self.waiting = True

            # we must wait a full 60 ticks before releasing
            if self.tick > 60:
                self.tick = 0
                self.throttle = None
                self.waiting = False
                self.detectcounter = 0

        return self.throttle


    def update(self):
        print("camera not ready! wait for stop sign")
        time.sleep(LOAD_TIME_WAIT_SEC)  # wait for camera to load
        self.ready = True  # set flag that camera is ready
        print("camera ready! starting stop sign")
        # this loop does stuff.


    def run(self, img_arr = None):
        return self.run_threaded()

    def run_threaded(self, img_arr = None):
        if self.ready: return self.detector(img_arr)
        else: return None


# Turn Sign Detector
class TrafficSignTurnDetector:
    '''
    Documentation to be added later

    '''

    def __init__(self, cfg, debug=False):
        self.angle = None
        self.detectcounter = 0
        self.mode = 'user'
        self.debug = debug
        self.ready = False
        self.tick = 0
        self.waiting = False
        self.action = None

    def shutdown(self):
        try:
            pass
        except:
            pass

    def detector(self, img_arr):
        '''
        Detects if there is a turn sign in given frame and then returns an angle value.
        '''
        # run the image through the detector
        result = detect_turn(img_arr)
        self.tick += 1

        # detections - add counter if yes
        if (result == "left" or result == "right") and not self.waiting:
            self.detectcounter += 1
            self.action = result

        # false detections - reset counter when no detections after 0.25 seconds
        if result == None and self.detectcounter < COUNTER_THRESHOLD and self.tick > 8:
            self.detectcounter = 0
            self.tick = 0
            self.angle = None
            self.action = None

        # positive detections - check for action
        if self.detectcounter >= COUNTER_THRESHOLD or self.waiting:
            # action for turning the car
            if self.action == "left":
                self.angle = -0.5   # turning left
            elif self.action == "right":
                self.angle = 0.5    # turning right

            self.waiting = True

            # we must wait a full 60 ticks before releasing (3 seconds)
            if self.tick > 60:
                self.tick = 0
                self.angle = None
                self.waiting = False
                self.detectcounter = 0
                self.action = None

        return self.angle


    def update(self):
        print("camera not ready! wait for turn sign")
        time.sleep(LOAD_TIME_WAIT_SEC)  # wait for camera to load
        self.ready = True  # set flag that camera is ready
        print("camera ready! starting turn sign")
        # this loop does stuff.


    def run(self, img_arr = None):
        return self.run_threaded()

    def run_threaded(self, img_arr = None):
        if self.ready: return self.detector(img_arr)
        else: return None


# Park Sign Detector
class TrafficSignParkDetector:
    '''
    Documentation to be added later

    '''

    def __init__(self, cfg, debug=False):
        self.angle = None
        self.throttle = None
        self.detectcounter = 0
        self.mode = 'user'
        self.debug = debug
        self.ready = False
        self.tick = 0
        self.waiting = False
        self.action = None

    def shutdown(self):
        try:
            pass
        except:
            pass

    def detector(self, img_arr):
        '''
        Detects if there is a turn sign in given frame and then returns an angle value.
        '''
        # run the image through the detector
        result = detect_park(img_arr)
        self.tick += 1

        # detections - add counter if yes
        if result == "park" and not self.waiting:
            self.detectcounter += 1

        # false detections - reset counter when no detections after 0.25 seconds
        if result == None and self.detectcounter < COUNTER_THRESHOLD and self.tick > 8:
            self.detectcounter = 0
            self.tick = 0
            self.angle = None
            self.throttle = None

        # positive detections - check for action
        if self.detectcounter >= COUNTER_THRESHOLD or self.waiting:
            # action for parking the car (over time)
            ##  To accomplish this, we have to send different values over various ticks
            ##   since using sleep freezes the Donkey Car.
            ##  We shall define ticks as 20 every second.  The whole process will take
            ##   120 ticks (approx) or 6 seconds
            ##
            if self.tick < 40:
                # first we must steer the car into the parking spot, assuming we know
                #  which side the park sign is on (TODO)
                self.angle = 1.0        # lock left)
                self.throttle = -0.5    # reverse parking?

            elif self.tick < 60:
                self.angle = 0.0        # straighten up
                self.throttle = -0.5    # reverse parking?

            elif self.tick > 100:
                self.angle = 0.0
                self.throttle = 0.0     # stop the car

            self.waiting = True

            # we must wait a full 200 ticks before releasing (10 seconds)
            if self.tick > 200:
                self.tick = 0
                self.angle = None
                self.waiting = False
                self.detectcounter = 0
                self.action = None

        return self.angle


    def update(self):
        print("camera not ready! wait for park sign")
        time.sleep(LOAD_TIME_WAIT_SEC)  # wait for camera to load
        self.ready = True  # set flag that camera is ready
        print("camera ready! starting park sign")
        # this loop does stuff.


    def run(self, img_arr = None):
        return self.run_threaded()

    def run_threaded(self, img_arr = None):
        if self.ready: return self.detector(img_arr)
        else: return None
